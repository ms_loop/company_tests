<?php

namespace Drupal\random_quote_test;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RandomQuoteRepository implements ContainerFactoryPluginInterface {

  /**
   * The http service.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Constructs a GuzzleHttp object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Client $httpClient) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->httpClient = $httpClient;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('http_client')
    );
  }

  /**
   * Get quote by url.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getQuote() {
    $request = $this->httpClient->request( 'GET', 'https://api.kanye.rest/', ['verify' => FALSE]);
    $json_data  = $request->getBody()->getContents();
    $data = (array) json_decode($json_data);

    return $data['quote'];
  }
}
