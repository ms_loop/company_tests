<?php

namespace Drupal\random_quote_test\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\random_quote_test\RandomQuoteRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Form for demonstration.
 */
class RandomQuoteForm extends FormBase {

  /**
   * Repository service.
   *
   * @var \Drupal\random_quote_test\RandomQuoteRepository
   */
  protected $repository;

  /**
   * Construct the new block object.
   */
  public function __construct(RandomQuoteRepository $repository) {
    $this->repository = $repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('random_quote.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'id_random_quote';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['random_quote'] = [
      '#type' => 'select',
      '#options' => [
        'quote' => $this->repository->getQuote(),
      ],
      '#attributes' => [
        'style' => 'width: 5em;'
      ],
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Update'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Nothing.
  }
}
