<?php

namespace Drupal\database_api_test;

/**
 * Provides the default database storage backend for statistics.
 */
class StatisticStorage {

  /**
   * Update and save node view records.
   *
   * @return bool
   */
  public static function recordNodeView($node_id, $user_id): bool {
    return (bool)\Drupal::database()
      ->merge('node_view_counter')
      ->key('nid', $node_id)
      ->fields([
        'uid' => $user_id,
        'total' => 1,
        'day' => 1,
        'timestamp' => self::getRequestTime(),
      ])
      ->expression('total', '[total] + 1')
      ->expression('day', '[day] + 1')
      ->execute();
  }

  /**
   * Get node with all fields by id.
   */
  public static function fetchNodeViews($node_id) {
    return \Drupal::database()
      ->select('node_view_counter', 'nc')
      ->fields('nc', ['uid', 'total', 'day', 'timestamp'])
      ->condition('nid', $node_id, 'IN')
      ->execute()
      ->fetchAll();
  }

  /**
   * Remove records after node's delete.
   */
  public static function deleteNodeViews($node_id): bool {
    return (bool)\Drupal::database()
      ->delete('node_view_counter')
      ->condition('nid', $node_id)
      ->execute();
  }


  /**
   * Get current request time.
   *
   * @return int
   *   Unix timestamp for current server request time.
   */
  protected static function getRequestTime(): int {
    return \Drupal::time()->getRequestTime();
  }

}
