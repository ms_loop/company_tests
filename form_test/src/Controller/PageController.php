<?php

namespace Drupal\form_test\Controller;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Simple page controller for drupal.
 */
class PageController {

  use StringTranslationTrait;

  /**
   * Build a simple page.
   *
   * @return array
   */
  public function getPage(): array
  {
    return [
      '#markup' => '<p>' . $this->t('Simple page.') . '</p>',
    ];
  }

}
