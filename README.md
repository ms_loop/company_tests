# Drupal 9 Tests

## 1. Working with views

- Create a view block that will list content of type Article, displayed as teaser view mode.
- Place this block in “Content” region of the Bartik theme, under main page content.
- Configure the above view to display articles created by the current logged in user (you will have to use contextual filters for this).
- By default in Drupal, article pages have a Tags field, add an exposed filter to the above view to filter by tag name. To accomplish this you will have to configure a relationship to tags field, also don’t forget to enable Ajax for this view block, without it the filter will not be displayed on the page.

@module: [views_test](https://gitlab.com/ms_loop/company_tests/-/tree/main/views_test)

## 2. Form API

- Create a custom page that will be accessible at “/custom-site-settings”.
- Create a form shown in the attached mockup.
- This form should show the current site name in the textfield and should save the new site name if the user decides to change it by clicking Save.
- This form should be available only to users with 'administer mymodule' permission.
- You can test with a simple user with a role that has that permission to verify if it is done correctly, don't test with user 1 (it has all permissions in the world).
- Don't allow users to submit site names with less than 6 characters length (validate their input).

**Useful links:**
- [Create a page](https://www.drupal.org/docs/creating-modules/create-a-custom-page-using-a-controller).
- [Create a custom form](https://www.drupal.org/docs/drupal-apis/form-api/introduction-to-form-api).

![alt text](https://snipboard.io/5MqiE1.jpg)

@module [form_test](https://gitlab.com/ms_loop/company_tests/-/tree/main/form_test)

## 3. Working with Drupal APIs

- Basically you will have to create a page similar to one displayed in mockup attached here.
- It will be a simple form with a select box that lists all 'article' nodes from site.
- When selecting an article and clicking Update, Article publishing options should be changed accordingly.
- When clicking delete, article will be deleted.

![alt text](https://snipboard.io/o0RmCE.jpg)

@module [drupal_api_test](https://gitlab.com/ms_loop/company_tests/-/tree/main/drupal_api_test)

## 4. Database API

- Take a look into db.png an try to implement a similar behaviour on node view pages.
- After node title, display number of views for this node for today, as well as total number of views.
- As second line display last viewed username and date.

**Here is what you need to know for that:**

1. We're starting new chapter - Database API in Drupal. Drupal has its own abstraction layer for DB queries, see [Database API Overview](https://www.drupal.org/docs/drupal-apis/database-api/database-api-overview).
2. You also need to know that Drupal uses it's schema API to create tables in DB and to update those tables, no need to create tables with phpmyadmin, see [Schema API](https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Database%21database.api.php/group/schemaapi/9.4.x).

- You will probably want to create a table with | nid | uid | timestamp | (and maybe others) columns and store all node views data there. Try to find out how to achieve this.
- Also you need to know that you can use hook_node_view() to hook in Drupal node displaying process.
- You'll have to do 2 things here: update $node variable to include statistics information and, insert new node view row into database.

![alt text](https://snipboard.io/CDw0YB.jpg)

@module [database_api_test](https://gitlab.com/ms_loop/company_tests/-/tree/main/database_api_test)

## 5. Dependency Injection and Services

**Scope:** During this section you will learn about services and dependency injection as a concept and learn to apply them. You will see the benefit of using services for pluggable architecture.

- Create random_quote module that will allow users to view a random quote in a block using some external quotes databases.
- Create RandomQuotes service that will return a random quote for every call.
- You can use a random quotes API available online.

**Random quotes APIs:**

- https://api.kanye.rest/
- https://api.chucknorris.io/jokes/random

Use any API you find appropriate.

- Create a block that will display that quote and make use of this service.

**Pass criteria:**

- Service is working properly and returning new quote for every call.
- Block is using the RandomQuotes service to display the quote.
- Make sure to inject the service in the Block constructor, don’t get it from the container.

**Hints:**

- Learn about Dependency Injection before proceeding, see [DI](http://fabien.potencier.org/what-is-dependency-injection.html).
- Learn about Service container in Symfony here before proceeding, see [Service container](https://symfony.com/doc/current/service_container.html).
- Learn about services in Drupal, see [Services](https://www.drupal.org/docs/drupal-apis/services-and-dependency-injection/services-and-dependency-injection-in-drupal-8).
- Inject services in Forms and Block (plugins) the right way, see [Forms and Block (plugins)](https://www.drupal.org/docs/drupal-apis/services-and-dependency-injection/dependency-injection-for-a-form).

@module [random_quote_test](https://gitlab.com/ms_loop/company_tests/-/tree/main/random_quote_test)

## License

MIT

**@Free Software**
