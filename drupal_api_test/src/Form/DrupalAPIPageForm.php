<?php

namespace Drupal\drupal_api_test\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\node\Entity\Node;

/**
 * Implements the container form.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class DrupalAPIPageForm extends FormBase {

  /**
   * The node storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * Constructs a PageForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->storage = $entity_type_manager->getStorage('node');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Getter method for Form ID.
   *
   * @return string
   *   The unique ID of the form.
   */
  public function getFormId(): string {
    return 'id_drupal_page_form';
  }

  /**
   * Build the simple form.
   *
   * @param array $form
   *   Default form array structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object containing current form state.
   *
   * @return array
   *   The render array defining the elements of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['container'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Control publishing options'),
      '#attributes' => [
        'style' => 'width: 16em;'
      ],
    ];
    $form['container']['select_node'] = [
      '#type' => 'select',
      '#options' => $this->getNodesTitle(),
      '#attributes' => [
        'style' => 'width: 10em;'
      ],
    ];
    $form['container']['select_publisher'] = [
      '#type' => 'select',
      '#options' => [
        'publisher' => $this->t('Publisher'),
        'draft' => $this->t('Draft'),
      ],
    ];
    $form['container']['select_sticky'] = [
      '#type' => 'select',
      '#options' => [
        'sticky' => $this->t('Sticky'),
        'unstick' => $this->t('Unstick'),
      ],
    ];
    $form['container']['actions'] = ['#type' => 'actions'];
    $form['container']['actions']['update'] = [
      '#type' => 'submit',
      '#name' => 'update',
      '#value' => $this->t('Update'),
    ];
    $form['container']['actions']['delete'] = [
      '#type' => 'submit',
      '#name' => 'delete',
      '#value' => $this->t('Delete'),
    ];

    return $form;
  }

  /**
   * Implements a form submit handler.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $node_id = $form_state->getValue('select_node');
    $is_publisher = $form_state->getValue('select_publisher');
    $is_sticky = $form_state->getValue('select_sticky');

    $node = Node::load($node_id);
    $trigger = $form_state->getTriggeringElement();

    switch($trigger['#name']) {
      case 'update':
        if ($is_publisher == 'publisher' && $is_sticky == 'sticky') {
          $node->isPublished() ?: $node->setPublished();
          $node->isSticky() ?: $node->setSticky(TRUE);
        }
        if ($is_publisher == 'draft' && $is_sticky == 'sticky') {
          !$node->isPublished() ?: $node->setUnpublished();
          $node->isSticky() ?: $node->setSticky(TRUE);
        }
        if ($is_publisher == 'draft' && $is_sticky == 'unstick') {
          !$node->isPublished() ?: $node->setUnpublished();
          !$node->isSticky() ?: $node->setSticky(FALSE);
        }
        if ($is_publisher == 'publisher' && $is_sticky == 'unstick') {
          $node->isPublished() ?: $node->setPublished();
          !$node->isSticky() ?: $node->setSticky(FALSE);
        }

        $node->save();
        $this->messenger()->addMessage($this->t('Node with ID=%nid change statuses %is_publisher | %is_sticky.',
            ['%nid' => $node_id, '%is_publisher' => $is_publisher, '%is_sticky' => $is_sticky]));
        break;

      case 'delete':
        $node->delete();
        $this->messenger()->addMessage($this->t('Deleted node with  ID=%nid.', ['%nid' => $node_id]));
        break;
    }
  }

  /**
   * Load Nodes id by Article content type.
   *
   * @return array
   */
  private function getNodesTitle(): array {
    $nids = $this->storage->getQuery()
      ->condition('type','article')
      ->execute();

    $nodes = $this->storage->loadMultiple($nids);
    $nodeTitles = [];
    foreach ($nodes as $node) {
      $nodeTitles[$node->id()] = $node->label();
    }

    return $nodeTitles;
  }
}

